# Makefile
#
# Makefile completo separando os diferentes elementos da aplicacao 
# como codigo (src), cabecalhos (include), executaveis (build),
# bibliotecas (lib), etc. Cada elemento e alocado em uma pasta
# especifica, organizando melhor o codigo fonte.
#
# Algumas variaveis sao usadas com significado especial:
#
# $@ nome do alvo (target)
# $^ lista com os nomes de todos os pre-requisitos sem duplicatas
# $< nome do primeiro pre-requisito
#

# Comandos do sistema operacional
# Linux: rm -rf 
# Windows: cmd //C del 
RM = rm -rf 

# Compilador
CC=g++

# Variaveis para os subdiretorios
INC_DIR=include
SRC_DIR=src
OBJ_DIR=build
BIN_DIR=bin
DOC_DIR=doc

# Elementos da STL
STL=vector list stack queue deque set multiset map multimap priority_queue algorithm1 algorithm2 algorithm3 numeric
 
# Opcoes de compilacao
CFLAGS=-Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)

# Garante que os alvos desta lista nao sejam confundidos com arquivos 
# de mesmo nome
.PHONY: all init clean debug doxy doc

# Define o alvo (target) para a compilacao completa e os alvos 
# de dependencia. Ao final da compilacao, remove os arquivos objeto.
all: init stl

debug: CFLAGS += -g -O0
debug: all

# Alvo (target) para a criação da estrutura de diretorios
# necessaria para a geracao dos arquivos objeto 
init:
	@mkdir -p $(BIN_DIR)/
	@mkdir -p $(OBJ_DIR)/

# Alvo (target) para a compilacao de cada um dos exemplos.
# É executado um loop sobre a lista definida na variável STL, gerando-se
# primeiramente o respectivo arquivo objeto no diretorio build e em seguida 
# respectivo executavel no diretorio bin. Os arquivos fonte, objeto e 
# executavel possuem todos os mesmos nomes definidos sequencialmente na 
# variável STL.
stl:
	@for c in $(STL); do \
		echo "Gerando executavel $(BIN_DIR)/$$c"; \
		$(CC) -c $(CFLAGS) -o $(OBJ_DIR)/$$c.o $(SRC_DIR)/$$c.cpp; \
		$(CC) $(CFLAGS) -o $(BIN_DIR)/$$c $(OBJ_DIR)/$$c.o; \
	done

# Alvo (target) para a geração automatica de documentacao 
# usando o Doxygen. Sempre remove a documentacao anterior (caso exista)
# e gera uma nova.
doxy:
	doxygen -g

doc:
	$(RM) $(DOC_DIR)/*
	doxygen

# Alvo (target) usado para limpar os arquivos temporarios (objeto)
# gerados durante a compilacao, assim como os arquivos
# binarios/executaveis.
clean:
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*
