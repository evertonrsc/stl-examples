/**
 * @file	queue.cpp
 * @brief	Demonstracao do uso do container queue
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @since	03/11/2016
 * @date	10/11/2016
 */

#include <iostream>
using std::cout;
using std::endl;

#include <queue>
using std::queue;

#include <string>
using std::string;

/** @brief Funcao principal */
int main() {
    queue<string> q;                    // Criacao de uma fila vazia de strings
    q.push("Roberto");                  // Insercao de elementos na fila
    q.push("Antonio");
    q.push("Maria");

    cout << "Elementos da fila: ";
    while (!q.empty()) {
       cout << q.front() << " ";        // Impressao do primeiro elemento da fila
       q.pop();                         // Remocao do primeiro elemento da fila
    }
	cout << endl;

    return 0;
}
