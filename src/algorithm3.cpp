/**
 * @file	algorithm3.cpp
 * @brief	Demonstracao do uso de algoritmos relacionados a ordenacao
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @since	03/11/2016
 * @date	18/05/2017
 */

#include <algorithm>
using std::binary_search;
using std::max_element;
using std::sort;

#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;

/**
 * @brief Funcao generica para imprimir os elementos de um vetor
 * @param v Vetor generico
 */
template<typename T>
void imprime(vector<T> v) {
	/* Iterador de tipo generico para o vetor
	 * Necessario adicionar a palavra-chave typename para informar ao
	 * compilador que T e um tipo */
	typename vector<T>::iterator i;

	cout << "Elementos do vetor: ";
	for (i = v.begin(); i != v.end(); ++i) {
		cout << *i << " ";
	}
	cout << std::endl;
}


/** @brief Funcao principal */
int main() {
   int num_array[] = { 12, 5, 21, 23, 44, 38 }; 

   // Criacao de um vetor de inteiros a partir do array
   vector<int> numeros(num_array, num_array + sizeof(num_array) / sizeof(int));

   // Retorna um iterador para o primeiro elemento com maior valor no vetor
   vector<int>::iterator it = max_element(numeros.begin(), numeros.end());
   cout << "Maior valor no vetor: " << *it << endl;

   // Ordena os tres primeiros elementos do vetor
   sort(numeros.begin(), numeros.begin()+3);
   imprime(numeros);

   // Ordena todo o vetor e realiza uma busca binaria pelo valor 38
   sort(numeros.begin(), numeros.end());
   if (binary_search(numeros.begin(), numeros.end(), 38)) {
       cout << "Valor encontrado" << endl;
   } else {
       cout << "Valor nao encontrado" << endl;
   }

   return 0;
}

