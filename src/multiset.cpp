/**
 * @file	multiset.cpp
 * @brief	Demonstracao do uso do container multiset
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @since	03/11/2016
 * @date	18/05/2017
 */

#include <iostream>
using std::cout;
using std::endl;

#include <set>
using std::multiset;
using std::pair;

#include <string>
using std::string;

/** @brief Funcao principal */
int main() {
	// Instanciacao de um multiset de strings
	multiset<string> frutas;
	frutas.insert("banana");
	frutas.insert("morango");
	frutas.insert("abacaxi");
	frutas.insert("uva");

	// Insercao de elementos repetidos
	frutas.insert("uva");
	frutas.insert("morango");
	frutas.erase("morango");			// Remocao de todas as ocorrencias do elemento

	// Impressao do conteudo do multiset usando iteradores
	multiset<string>::iterator it;
	cout << frutas.size() << " elementos no conjunto: { ";
	for (it = frutas.begin(); it != frutas.end(); ++it) {
		cout << *it << " ";
	}
	cout << "}" << endl;

	return 0;
}


