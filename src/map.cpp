/**
 * @file	map.cpp
 * @brief	Demonstracao do uso do container map
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @since	03/11/2016
 * @date	18/05/2017
 */

#include <iostream>
using std::cout;
using std::endl;

#include <map>
using std::map;
using std::pair;

#include <string>
using std::string;

/** @brief Funcao principal */
int main() {
	// Cria um mapa vazio com chaves int e valores string
    map<int, string> alunos;

	// Insercao de pares chave-valor utilizando o metodo insert
    alunos.insert(pair<int, string>(1, "Maria"));
    alunos.insert(pair<int, string>(2, "Joao"));

	// Insercao de valores com chaves 2 e 3 utilizando operador []
    alunos[3] = "Ana";
	alunos[2] = "Lucas";
	
	// Remocao de valor com chave 1
    alunos.erase(1);
	
	// Impressao dos valores no mapa e suas chaves usando iteradores
    map<int, string>::iterator it;
    for (it = alunos.begin(); it != alunos.end(); ++it) {
       cout << it->first << " - " << it->second << endl;
	}

	// Busca por elemento no mapa utilizando chave
    map<int, string>::iterator busca = alunos.find(1);
	if (busca != alunos.end()) {
	    cout << "Codigo: " << busca->first << " - Nome: " << busca->second << endl;
	}
	
    return 0;
}
