/**
 * @file	priority_queue.cpp
 * @brief	Demonstracao do uso do container priority_queue
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @since	03/11/2016
 * @date	10/11/2016
 */

#include <iostream>
using std::cout;
using std::endl;

#include <deque>
using std::deque;

#include <queue>
using std::priority_queue;

/** @brief Funcao principal */
int main() {
	// Fila implementada em um vector (default)
	priority_queue<int> fila;

	// Fila implementada em um deque
	// Nota: usar espaco entre "> >" para diferenciar de ">>"
	priority_queue<int,deque<int> > fila_lista; 

	if (fila.empty()) {
		cout << "Fila vazia." << endl;
	}
		
	// Insercao de elementos
	fila.push(55);
	fila.push(5);
	fila.push(16);
	fila.push(98);
	cout << "Total de elementos: " << fila.size() << endl;

	// Remocao do elemento com maior prioridade
	fila.pop();
	cout << "Elemento com maior prioridade: " << fila.top() << endl;

	return 0;
}

