/**
 * @file	stack.cpp
 * @brief	Demonstracao do uso do container stack
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @since	03/11/2016
 * @date	10/11/2016
 */

#include <iostream>
using std::cout;
using std::endl;

#include <stack>
using std::stack;

/** @brief Funcao principal */
int main() {
    stack<int> s;                   // Criacao de uma pilha vazia de inteiros
    for (int i = 1; i <= 5; i++) {
       s.push(i);					// Insercao de elementos na pilha
    }

    cout << "Elementos da pilha: ";
    while (!s.empty()) {
       cout << s.top() << " ";      // Impressao do elemento que esta no topo da pilha
       s.pop();                     // Remocao do elemento que esta no topo da pilha
    }
	cout << endl;

    return 0;
}
