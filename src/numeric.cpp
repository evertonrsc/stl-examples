/**
 * @file	numeric.cpp
 * @brief	Demonstracao do uso de algoritmos numericos genericos
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @since	03/11/2016
 * @date	10/11/2016
 */

#include <functional>
using std::multiplies;

#include <iostream>
using std::cout;
using std::endl;

#include <numeric>
using std::accumulate;

#include <vector>
using std::vector;

/** @brief Funcao principal */
int main() {
   vector<int> numeros;
   for (int i = 1; i <= 5; i++) {
      numeros.push_back(i);
   }

   // Calcula o acumulado (somatorio) dos valores no vetor
   int soma = accumulate(numeros.begin(), numeros.end(), 0);
   cout << "Soma dos elementos do vetor: " << soma << endl;

   // Calcula o acumulado (produtorio) dos valores no vetor
   int produto = accumulate(numeros.begin(), numeros.end(), 1, multiplies<int>());
   cout << "Produto dos elementos do vetor: " << produto << endl;

   return 0;
}

