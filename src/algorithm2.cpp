/**
 * @file	algorithm2.cpp
 * @brief	Demonstracao do uso de algoritmos de modificacao de sequencias
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @since	03/11/2016
 * @date	10/11/2016
 */

#include <algorithm>
using std::fill;
using std::random_shuffle;
using std::replace;
using std::reverse;

#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;

/** 
 * @brief Funcao para imprimir os elementos de um vetor
 * @param v Vetor de inteiros
 */
void imprime(vector<int> v) {
   vector<int>::iterator i;
   for (i = v.begin(); i != v.end(); ++i) {
      cout << *i << " ";
   }
   cout << endl;
}

/** @brief Funcao principal */
int main() {
    vector<int> numeros;            	// Criacao de um vetor de inteiros vazio
    for (int i = 1; i <= 10; i++) {
       numeros.push_back(i);            // Insercao de elementos no vetor
    }

    // Inverte a ordem dos elementos no vetor
    reverse(numeros.begin(), numeros.end());
    imprime(numeros);

    // Reordena aleatoriamente os elementos do vetor
    random_shuffle(numeros.begin(), numeros.end());
    imprime(numeros);

    // Substitui a ocorrencia de um valor por outro
    replace(numeros.begin(), numeros.end(), 10, 100);
    imprime(numeros);

    // Preenche todos os elementos do vetor com 0
    fill(numeros.begin(), numeros.end(), 0);
    imprime(numeros);

    return 0;
}

