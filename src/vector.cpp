/**
 * @file	vector.cpp
 * @brief	Demonstracao do uso do container vector
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @since	03/11/2016
 * @date	18/05/2017
 */

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;

#include <vector>
using std::vector;

/**
 * @brief Funcao generica para imprimir os elementos de um vetor
 * @param v Vetor generico
 */
template<typename T>
void imprime(vector<T> v) {
	cout << "Elementos do vetor: ";
	for (int i = 0; i < (int)v.size(); i++) {   // Iteracao sobre os elementos do vetor
       cout << v[i] << " ";                		// Acesso ao elemento indicado
    }
	cout << endl;
}

/** @brief Funcao principal */
int main() {
    vector<int> v;                      // Criacao de um vetor de inteiros vazio
    for (int i = 1; i <= 10; i++) {
       v.push_back(i);             		// Insercao de elementos no vetor
    }

    imprime(v);		// Impressao dos elementos do vetor

    return 0;
}
