/**
 * @file	deque.cpp
 * @brief	Demonstracao do uso do container deque
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @since	03/11/2016
 * @date	10/11/2016
 */

#include <deque>
using std::deque;

#include <iostream>
using std::cout;
using std::endl;

/** @brief Funcao principal */
int main() {
	// Criacao de um deque vazio de inteiros
    deque<int> d;
    
	// Insercao de elementos no inicio do deque (0 a 5)
	for (int i = 5; i >= 0; i--) {
		d.push_front(i);
	}
    
	// Insercao de elementos no final do deque (6 a 11)
	for (int i = 6; i <= 11; i++) {
		d.push_back(i);
    }

    d.pop_back();	// Remocao do ultimo elemento do deque
    d.pop_front();  // Remocao do primeiro elemento do deque
    
    deque<int>::iterator it;						// Iterador sobre o deque
    for (it = d.begin(); it != d.end(); ++it) {
        std::cout << *it << " ";                    // Impressao dos elementos do deque
    }
	cout << endl;

    return 0;
}
