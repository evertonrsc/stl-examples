/**
 * @file	algorithm1.cpp
 * @brief	Demonstracao do uso de algoritmos de nao-modificacao de sequencias
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @since	03/11/2016
 * @date	10/11/2016
 */

#include <algorithm>
using std::count;
using std::find;
using std::for_each;

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;

#include <vector>
using std::vector;


/** 
 * @brief Funcao para impressao de string
 * @param nome String a ser impressa
 */
void imprime(string nome) { 
	cout << nome << " "; 
}

/** @brief Funcao principal */
int main() {
	// Criacao de um vetor de strings com quatro elementos
    vector<string> nomes(4);
    nomes[0] = "Paulo";
    nomes[1] = "Joao";
    nomes[2] = "Mauricio";
    nomes[3] = "Joao";

    // Conta o numero de ocorrencias de "Joao" no vetor
    int ocorrencias = std::count(nomes.begin(), nomes.end(), "Joao");
    cout << "Foram encontradas " << ocorrencias << " ocorrencias no vetor" << endl;

    // Encontra a primeira ocorrência de "Joao" no vetor
    vector<string>::iterator it;
    it = find(nomes.begin(), nomes.end(), "Joao");
    if (it == nomes.end()) {
       cout << "String nao encontrada" << endl;
    }

    // Chama a funcao imprime para cada elemento do vetor
    for_each(nomes.begin(), nomes.end(), imprime);
	cout << endl;

    return 0;
}
