/**
 * @file	list.cpp
 * @brief	Demonstracao do uso do container list
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @since	03/11/2016
 * @date	18/05/2017
 */

#include <iostream>
using std::cout;
using std::endl;

#include <list>
using std::list;

/** @brief Funcao principal */
int main() {
    list<int> l;                // Criacao de uma lista vazia de inteiros
    
    l.push_front(1);            // Insercao de elemento no inicio da lista
    l.push_back(2);             // Insercao de elemento no final da lista

	list<int>::iterator it = l.begin();	// Iterador para o inicio da lista
    l.insert(it, 0);					// Insercao de elemento na posicao do iterador

    for (it = l.begin(); it != l.end(); it++) {	// Iteracao sobre a lista
       cout << *it << " ";						// Elemento apontado pelo iterador
	}
	cout << endl;

    return 0;
}
