/**
 * @file	multimap.cpp
 * @brief	Demonstracao do uso do container multimap
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @since	03/11/2016
 * @date	18/05/2017
 */

#include <iostream>
using std::cout;
using std::endl;

#include <map>
using std::multimap;
using std::pair;

#include <string>
using std::string;

/** @brief Funcao principal */
int main() {
	// Instanciacao de multimap de chaves int e valor string
	multimap<int, string> alunos;

	// Insercao de elementos no multimap
	alunos.insert(pair<int, string>(5,"Joao"));
	alunos.insert(pair<int, string>(2,"Ana"));
	alunos.insert(pair<int, string>(1,"Ziraldo"));
	alunos.insert(pair<int, string>(3,"Ana"));

	// Tentativa de inserir um registro com a mesma chave
	alunos.insert(pair<int, string>(5,"Joana"));

	// Busca pelo registro com chave com valor "5"
	multimap<int, string>::iterator busca = alunos.find(5);
	cout << "Codigo: " << busca->first << " - Nome: " << busca->second << endl;

	// Remocao do registro com chave "5"
	int removidos = alunos.erase(5);
	cout << "Foram removidos " << removidos << " registros" << endl;

	// Impressao do conteudo do multimap utilizando iteradores
	multimap<int,string>::iterator it;
	for (it = alunos.begin(); it != alunos.end(); ++it) {
		cout << "(" << it->first << ", " << it->second << ") ";
	}
	cout << endl;

	return 0;
}


