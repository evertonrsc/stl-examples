/**
 * @file	set.cpp
 * @brief	Demonstracao do uso do container set
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @since	03/11/2016
 * @date	18/05/2017
 */

#include <iostream>
using std::cout;
using std::endl;

#include <set>
using std::set;

#include <string>
using std::string;

/** 
 * @brief Funcao generica para buscar por um elemento em um container set
 * @param s Conjunto generico
 * @param e Elemento a ser buscado
 * @return Verdadeiro, caso o elemento esteja no conjunto, falso caso contrario
 */
template<typename T>
bool busca(set<T> s, T e) {
	/* Iterador de tipo generico para o conjunto
	 * Necessario adicionar a palavra-chave typename para informar ao
	 * compilador que T e um tipo */
	typename set<T>::iterator it;

	// Busca por elemento no conjunto
	it = s.find(e);
	return (it != s.end());
}

/** @brief Funcao principal */
int main() {
    set<string> frutas;      	// Criacao de um conjunto de strings vazio
    frutas.insert("banana");    // Insercao de elementos no conjunto
    frutas.insert("morango");
    frutas.insert("abacaxi");
	frutas.insert("banana");	// A insercao deste elemento sera ignorada, pois
								// o container nao permite duplicatas
        
	// Busca por elemento no conjunto
    if (busca(frutas, string("morango"))) {
        cout << "Elemento \"morango\" encontrado" << endl;
    } else {
        cout << "Elemento \"morango\" nao encontrado" << endl;
    }

	// Impressao dos elementos presentes no conjunto usando iteradores
	set<string>::iterator it;
	cout << frutas.size() << " elementos no conjunto: { ";
	for (it = frutas.begin(); it != frutas.end(); ++it) {
		cout << *it << " ";
	}
	cout << "}" << endl;

    return 0;
}

